import express from "express";

let app = express();


class Conversor{

    valorORIGINAL;
    valorCONVERTIDO;
    
    constructor(valorORIGINAL, valorCONVERTIDO){

        this.valorORIGINAL = valorORIGINAL;
        this.valorCONVERTIDO = valorCONVERTIDO;

    }

    textoAENTERO(){

        this.valorORIGINAL = parseFloat(this.valorORIGINAL);

    }
    enteroATEXTO(){

        this.valorCONVERTIDO = parseFloat(this.valorCONVERTIDO);

    }
}

class ConversorMedidas extends Conversor{


    piesAMETROS(){

    this.valorCONVERTIDO = (this.valorORIGINAL)/(3.2808);

    }   
    
    millasAKilometros(){
        this.valorCONVERTIDO = (this.valorORIGINAL)/(0.62137);

    }

}

class ConversorTemperatura extends Conversor{

    celsiusAFahrenheit(){

        this.valorCONVERTIDO = ((this.valorORIGINAL*9)/(5+32));
     

    }
    kelvinACelsius(){

        this.valorCONVERTIDO = ((this.valorORIGINAL)-273);

    }

}



app.set();

app.get("/piesAMETROS",(req, res)=>{

let miConversorMedidas = new ConversorMedidas();

miConversorMedidas.valorORIGINAL = 40;

res.send();

});



app.listen(8080);